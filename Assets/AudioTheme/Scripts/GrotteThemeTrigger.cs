using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrotteThemeTrigger : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player" && collision.gameObject.layer == LayerMask.NameToLayer("Player")) // the player enter the zone
        {
            Debug.Log(collision.name);
            FindObjectOfType<ThemeAudioManager>().Play("ThemeGrotte");
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player" && collision.gameObject.layer == LayerMask.NameToLayer("Player")) // the player exits the zone
        {
            //FindObjectOfType<ThemeAudioManager>().Stop("ThemeGrotte");
        }

    }
}
