using UnityEngine.Audio;
using UnityEngine;
using System;
using System.Collections;
using Pixelplacement;

public class ThemeAudioManager : MonoBehaviour
{

    public Sound[] sounds;
    Sound actualSound = null;
    public float fadeTime = 1f; //fade time in seconds

    public static ThemeAudioManager instance;

    void Awake() // Awake right before start
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }

        // audio manager persistent throughout scenes
        DontDestroyOnLoad(gameObject);

        foreach (Sound s in sounds)
        {
            
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.playOnAwake = false;
            s.source.clip = s.clip;
            s.source.Stop();
            s.source.loop = s.loop;
            s.source.panStereo = s.panStereo;

            // control volume and pitch
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
        }

    }


    // valueVolume range [-1f, 1f]
    public void ChangeVolume(string name, float valueVolume)
    {
       Sound s = Array.Find(sounds, sound => sound.name == name); // find the sound where sound.name == name
        if (s == null)
        {
            Debug.LogWarning("Audio file " + name + " not found!");
            return;
        }
        float newVolume = s.volume + valueVolume;

        if (newVolume > 1f)
            newVolume = 1f;
        else if (newVolume < 0)
            newVolume = 0;

        s.source.volume = newVolume;
    }

    void Start()
    {
        //Play("Theme");
    }

    public void Play(string name)
    {
        Debug.Log("CACA : " + name);
        float delay = 0;
        if (actualSound != null)
        {
            if (actualSound.name == name)
                return;
            Debug.Log("fade out : " + actualSound.name);
            Tween.FinishAll();
            Tween.Volume(actualSound.source, 0, fadeTime, 0);
            delay = fadeTime;
        }
        Sound s = Array.Find(sounds, sound => sound.name == name); // find the sound where sound.name == name
        actualSound = s;
        Debug.Log("find sound : " + s.name);


        if (s == null)
        {
            Debug.LogWarning("Audio file " + name + " not found!");
            return;
        }
        if (!s.source.isPlaying)
        {
            Debug.Log("Fade in : " + s.name);
            s.source.volume = 0;
            s.source.Play();
            actualSound = s;
            Tween.Volume(actualSound.source, 1, fadeTime, delay);
        }
    }

    public void Stop(string name)
    {
        foreach (Sound sound in sounds)
        {
            if (sound.source.isPlaying)
                sound.source.Stop();
        }
       /* Sound s = Array.Find(sounds, sound => sound.name == name); // find the sound where sound.name == name
        if (s == null)
        {
            Debug.LogWarning("Audio file " + name + " not found!");
            return;
        }
        s.source.Stop();*/
    }
}