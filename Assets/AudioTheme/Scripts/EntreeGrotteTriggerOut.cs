using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntreeGrotteTriggerOut : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player" && collision.gameObject.layer == LayerMask.NameToLayer("Player")) // the player enter the zone
        {
            //FindObjectOfType<ThemeAudioManager>().Stop("EntreeGrotte");
            FindObjectOfType<ThemeAudioManager>().Play("ThemeGrotte");
        }

    }
}
