using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Zenject;

public class RespawnPoint : MonoBehaviour
{
    [SerializeField] private UnityEngine.Rendering.Universal.Light2D _respawnLight;

    Animator animator;

    [Inject] readonly SignalBus signalBus;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        if (!_respawnLight)
            _respawnLight = GetComponent<UnityEngine.Rendering.Universal.Light2D>();

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            animator.SetTrigger("Activate");
            _respawnLight.intensity = 1;
            signalBus.Fire(new SetNewSpawn(gameObject));
        }
    }
}
