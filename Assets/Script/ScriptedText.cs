using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScriptedText : MonoBehaviour
{
    private TextMeshPro _text;
    private BoxCollider2D _collider;
    public bool isErased = true;
    [SerializeField] private float _timeOfFading = 1f;
    [SerializeField] private float _finalAlpha = 0.4f;


    private void Awake()
    {
        _text = GetComponent<TextMeshPro>();
        _collider = GetComponent<BoxCollider2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            StopAllCoroutines();
            StartCoroutine(FadeIn());
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (isErased && collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            StopAllCoroutines();
            StartCoroutine(FadeOut());
        }

    }

    private IEnumerator FadeOut()
    {
        float elapsedTime = 0f;
        float delta = _finalAlpha / _timeOfFading;

        while (elapsedTime <= _timeOfFading)
        {
            elapsedTime = elapsedTime + Time.deltaTime;
            _text.color = new Color(1f, 1f, 1f, _text.color.a - delta * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator FadeIn()
    {
        float elapsedTime = 0f;
        float delta = _finalAlpha / _timeOfFading;

        while (elapsedTime <= _timeOfFading)
        {
            elapsedTime = elapsedTime + Time.deltaTime;
            _text.color = new Color(1f, 1f, 1f, _text.color.a + delta * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
    }
}