using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;
using Zenject;

public class AimingManager : MonoBehaviour
{

    private GlowstickManager _glowstickManager;

    [SerializeField] Transform _glowstickParent;

    [SerializeField, ReadOnly]
    private GameObject _glowstickToInstantiate;

    [SerializeField, ReadOnly]
    private float _maxVelocity = 10f;
    [SerializeField, ReadOnly]
    private int _numberOfPointOfAiming = 100;
    [SerializeField, ReadOnly]
    private float _aimingSimulationLenght = 1f;

    private Vector2 _mousePosition;
    private Camera _mainCamera;
    private LineRenderer _aimingLine;
    private bool _isAiming = false;

    public bool IsAiming => _isAiming;

    private void Awake()
    {
        _glowstickManager = FindObjectOfType<GlowstickManager>();
    }

    private void Start()
    {
        _mainCamera = Camera.main;
        _aimingLine = GetComponentInChildren<LineRenderer>();
        if (!_aimingLine)
            Debug.LogWarning("No AimingLine attached to Player");
        else
        {
            _aimingLine.positionCount = _numberOfPointOfAiming;
            Gradient gradient = new Gradient();
            gradient.SetKeys(
                new GradientColorKey[] { new GradientColorKey(Color.blue, 0.0f), new GradientColorKey(Color.red, 1.0f) },
                new GradientAlphaKey[] { new GradientAlphaKey(1f, 0.0f), new GradientAlphaKey(0f, 1.0f) }
            );
            _aimingLine.colorGradient = gradient;
        }
    }

    public void OnPosition(InputAction.CallbackContext context)
    {
        if (_mainCamera != null)
        {
            _mousePosition = _mainCamera.ScreenToWorldPoint(context.ReadValue<Vector2>());
            if (_isAiming)
            {
                DrawAimingLine();
            }
        }

    }


    public void DrawAimingLine()
    {
        Vector2 initialVelocity = ((Vector2)transform.position - _mousePosition);
        if (initialVelocity.magnitude >= _maxVelocity)
            initialVelocity = initialVelocity / initialVelocity.magnitude * _maxVelocity;
        float deltaTime = _aimingSimulationLenght / _numberOfPointOfAiming;
        float totalTime = 0f;
        for (int i = 0; i < _numberOfPointOfAiming; i++)
        {
            Vector3 newPosition = new Vector3(initialVelocity.x * totalTime, initialVelocity.y * totalTime + totalTime * totalTime * Physics2D.gravity.y / 2f, 0f) + _aimingLine.transform.position;
            _aimingLine.SetPosition(i, newPosition);
            totalTime += deltaTime;
        }
    }



    public void OnClick(InputAction.CallbackContext context)
    {
        bool isPressed = context.ReadValueAsButton();
        if (isPressed && _glowstickManager.NumberRemaining > 0 && _glowstickManager.CurrentGlowstickCooldown <= 0.01f)
        {
            _aimingLine.positionCount = _numberOfPointOfAiming;
            _isAiming = true;
        }
        else if (_isAiming)
        {
            GameObject newGlowstick = Instantiate(_glowstickToInstantiate, _aimingLine.transform.position, _aimingLine.transform.rotation, _glowstickParent);

            _glowstickManager.FireGlowsticks();
            var glowstickRigidbody2D = newGlowstick.GetComponent<Rigidbody2D>();
            Vector2 initialVelocity = ((Vector2)transform.position - _mousePosition);
            if (initialVelocity.magnitude >= _maxVelocity)
                initialVelocity = initialVelocity / initialVelocity.magnitude * _maxVelocity;
            glowstickRigidbody2D.velocity = initialVelocity;
            _aimingLine.positionCount = 0;

            _isAiming = false;
        }

    }

    public void OnRightClick(InputAction.CallbackContext context)
    {
        _aimingLine.positionCount = 0;
        _isAiming = false;
    }
}
