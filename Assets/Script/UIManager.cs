using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private Slider _sliderLighter;
    [SerializeField] private TextMeshProUGUI _textGlowsticks;
    [SerializeField] private Image _glowStickImage;

    private void Awake()
    {
        if (!_sliderLighter)
            _sliderLighter = GetComponentInChildren<Slider>();
        if (!_textGlowsticks)
            _textGlowsticks = GetComponentInChildren<TextMeshProUGUI>();
        if (!_glowStickImage)
            _glowStickImage = GameObject.Find("GlowstickImage").GetComponent<Image>();
    }

    public void SetGlowsticksRemaining(int numberOfGlowsticks)
    {
        _textGlowsticks.text = "X " + numberOfGlowsticks.ToString();
    }
    public void SetLighterRemaining(float lighterValue)
    {
        _sliderLighter.value = lighterValue;
    }

    public void SetGlowstickGrey(float grey)
    {
        _glowStickImage.color = new Color(grey, grey, grey);
    }
}
