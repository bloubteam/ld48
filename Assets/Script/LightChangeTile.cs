using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LightChangeTile : MonoBehaviour
{
    [SerializeField]
    UnityEvent onTrigger;

    [SerializeField]
    LayerMask layerMask;

    [SerializeField] private float _newLightCharacterIntensity = 1f;
    [SerializeField] private float _newLightGlobalIntensity = 1f;

    private LightManager _lightManager;

    // Start is called before the first frame update
    void Awake()
    {
        GetComponent<Collider2D>().isTrigger = true;
        _lightManager = FindObjectOfType<LightManager>();
    }
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (((layerMask.value & (1 << collision.gameObject.layer)) > 0))
        {
            _lightManager.ChangeLightIntensity(_newLightGlobalIntensity, _newLightCharacterIntensity);
        }
    }
}
