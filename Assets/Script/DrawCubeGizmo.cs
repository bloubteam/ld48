using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawCubeGizmo : MonoBehaviour
{
    [SerializeField]
    Color color;

    [SerializeField]
    Vector3 size = Vector3.one;
    void OnDrawGizmos()
    {
        Gizmos.color = color;
        Gizmos.DrawWireCube(transform.position, size);
    }
}
