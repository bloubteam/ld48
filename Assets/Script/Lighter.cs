using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Lighter : MonoBehaviour
{
    [SerializeField] private float _useOfLighterFuelBySeconds = 0.1f;
    [SerializeField] private UnityEngine.Rendering.Universal.Light2D _lighterLight;
    [SerializeField] private float _lighterOffIntensity = 0f;
    [SerializeField] private float _lighterOnIntensity = 1f;
    [SerializeField] private UIManager _uiManager;
    private bool _lighterIsOn = false;
    private float _lighterFuelRemaining = 1f;



    private void Awake()
    {
        if (!_uiManager)
            _uiManager = FindObjectOfType<UIManager>();
        if (!_lighterLight)
            _lighterLight = GetComponentInChildren<UnityEngine.Rendering.Universal.Light2D>();
        _lighterLight.intensity = _lighterOffIntensity;
    }

    public void LighterSwitch()
    {

        if (_lighterIsOn == false && _lighterFuelRemaining > 0f)
        {
            _lighterIsOn = true;
            _lighterLight.intensity = _lighterOnIntensity;
        }
        else
        {
            _lighterIsOn = false;
            _lighterLight.intensity = _lighterOffIntensity;
        }

    }

    public void SetFuel(float newFuelQuantity)
    {
        _lighterFuelRemaining = newFuelQuantity;
        if (_lighterFuelRemaining < 0)
        {
            _lighterFuelRemaining = 0f;
            _lighterIsOn = false;
            _lighterLight.intensity = _lighterOffIntensity;
        }
        _uiManager.SetLighterRemaining(newFuelQuantity);
    }

    private void Update()
    {
        if (_lighterIsOn)
        {
            SetFuel(_lighterFuelRemaining - Time.deltaTime * _useOfLighterFuelBySeconds);
        }
    }
}
