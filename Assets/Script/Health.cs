using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

public class Health : MonoBehaviour
{
    [SerializeField]
    int maxHP = 1;

    [SerializeField]
    UnityEvent onDie;

    public bool IsAlive => currentHP > 0;

    int currentHP;

    public void Increment()
    {
        currentHP = Mathf.Clamp(currentHP + 1, 0, maxHP);
    }

    public void Decrement()
    {
        currentHP = Mathf.Clamp(currentHP - 1, 0, maxHP);
        if (currentHP == 0)
        {
            //var ev = Schedule<HealthIsZero>(); // TODO
            //ev.health = this; // TODO
        }
    }

    public void Die()
    {
        while (currentHP > 0) Decrement();
        onDie.Invoke();
    }

    void Awake()
    {
        currentHP = maxHP;
    }
}
