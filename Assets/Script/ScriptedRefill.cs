using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using Zenject;

public class ScriptedRefill : MonoBehaviour
{
    [Inject] GlowstickManager _glowstickManager;

    private BoxCollider2D _collider;
    [SerializeField, ReadOnly] private float _fuelToRefill = 1f;
    [SerializeField, ReadOnly] private int _glowstickToRefill = 5;

    private void Awake()
    {
        _collider = GetComponent<BoxCollider2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            Player player = collision.gameObject.GetComponent<Player>();
            if (player != null)
            {
                player.PlayerLighter.SetFuel(_fuelToRefill);
                _glowstickManager.RefillGlowsticks(_glowstickToRefill);
            }

        }
    }
}
