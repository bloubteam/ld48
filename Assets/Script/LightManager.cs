using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class LightManager : MonoBehaviour
{
    [SerializeField] private UnityEngine.Rendering.Universal.Light2D _globalLight;
    [SerializeField] private UnityEngine.Rendering.Universal.Light2D _characterLight;
    [SerializeField] private float _fadeOutTime = 1f;

    private bool _isFading = false;

    private void Awake()
    {
        if (!_characterLight)
            _characterLight = transform.Find("CharacterLight").GetComponent<UnityEngine.Rendering.Universal.Light2D>();
        if (!_globalLight)
            _globalLight = transform.Find("CharacterLight").GetComponent<UnityEngine.Rendering.Universal.Light2D>();
    }

    public void ChangeLightIntensity(float newGlobalIntensity, float newCharacterIntensity)
    {
        if (!_isFading)
        {
            if (newGlobalIntensity == 0f)
            {
                Camera.main.backgroundColor = Color.black;
            }
            _isFading = true;
            StartCoroutine(FadeOut(newGlobalIntensity, newCharacterIntensity));
        }
        
    }

    private IEnumerator FadeOut(float newGlobalIntensity, float newCharacterIntensity)
    {
        float elapsedTime = 0f;
        float globalDeltaIntensity = (_globalLight.intensity - newGlobalIntensity) / _fadeOutTime;
        float characterDeltaIntensity = (_characterLight.intensity - newCharacterIntensity) / _fadeOutTime;

        while (elapsedTime <= _fadeOutTime)
        {
            elapsedTime = elapsedTime + Time.deltaTime;
            _globalLight.intensity -= globalDeltaIntensity * Time.deltaTime;
            _characterLight.intensity -= characterDeltaIntensity * Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        _isFading = false;
    }
}
