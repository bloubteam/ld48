using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Glowstick : MonoBehaviour
{
    [SerializeField]
    private float _timeExpectancy = 5f;
    [SerializeField]
    private float _fadeOutTime = 1f;

    private ParticleSystem _particles;
    private UnityEngine.Rendering.Universal.Light2D _glowLight;
    private Collider2D _collider2D;
    private float _timeElapsed;
    private bool _isLit;

    private void Awake()
    {
        if (!TryGetComponent(out _glowLight))
        {
            Debug.LogError("No Light Component attached to Glowstick " + name);
            Destroy(gameObject);
        }
        if (!TryGetComponent(out _collider2D))
        {
            Debug.LogError("No Collider Component attached to Glowstick " + name);
            Destroy(gameObject);
        }
        _particles = GetComponentInChildren<ParticleSystem>();
        if (!_particles)
            Debug.LogWarning("No ParticleSystem Component attached to Glowstick " + name);
        _isLit = true;
    }

    private void Start()
    {

        _timeElapsed = 0f;
    }
    private void Update()
    {
        _timeElapsed += Time.deltaTime;
        if (_isLit && _timeElapsed >= _timeExpectancy)
        {
            StartCoroutine(FadeOut());
            _isLit = false;
        }
    }

    private IEnumerator FadeOut()
    {
        float elapsedTime = 0f;
        float deltaIntensity = _glowLight.intensity / _fadeOutTime;

        while (elapsedTime <= _fadeOutTime)
        {
            elapsedTime = elapsedTime + Time.deltaTime;
            _glowLight.intensity -= deltaIntensity * Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        if (_particles)
        {
            _particles.Stop();
        }

    }
}
