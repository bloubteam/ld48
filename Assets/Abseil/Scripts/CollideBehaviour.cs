using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CollideBehaviour : MonoBehaviour
{
    [SerializeField]
    UnityEvent onTrigger;

    Rigidbody2D r2d;

    private void Start()
    {
        r2d = GetComponent<Rigidbody2D>();
    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.relativeVelocity.magnitude > 15)
        {
            onTrigger.Invoke();
        }
    }
    
}
