using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Abseil : MonoBehaviour
{
    [SerializeField]
    LayerMask layerMask;

    [SerializeField]
    float rateOfFlowRopeOnWall = 1f;

    [SerializeField]
    float wallMagnete = 5f;

    [SerializeField]
    AnimationCurve rateOfFlowRopeByTime;

    [SerializeField]
    AnimationCurve jumpVelocityByTime;

    [SerializeField]
    GameObject attach;

    [SerializeField]
    GameObject abseilPlayer;

    [SerializeField]
    SpringJoint2D spring;

    [SerializeField]
    Rigidbody2D r2d;

    [SerializeField]
    Collider2D playerCollider;

    [SerializeField]
    Collider2D wallDetector;

    [SerializeField]
    Transform start;

    [SerializeField] AimingManager _aimingManager;

    Vector2 moveButton = Vector2.zero;

    public void OnMove(InputAction.CallbackContext context)
    {
        moveButton = context.ReadValue<Vector2>();
    }

    bool jumpButton = false;
    public void OnJump(InputAction.CallbackContext context)
    {
        if (context.started)
        {
            jumpButton = true;
        }
        if (context.canceled)
        {
            jumpButton = false;
        }
    }

    Dictionary<Rigidbody2D, Vector2> savedPositions;
    // Start is called before the first frame update
    void Start()
    {
        savedPositions = new Dictionary<Rigidbody2D, Vector2>();
        Rigidbody2D[] r2ds = GetComponentsInChildren<Rigidbody2D>();
        foreach (Rigidbody2D tr2d in r2ds)
            savedPositions.Add(tr2d, tr2d.position);
    }

    Player savedPlayer;

    enum CycleState {Running, ToDeactivate, ToReset, ToActivate };
    CycleState cycleState = CycleState.Running;

    void OnTriggerEnter2D(Collider2D collision) // change en used
    {
        Player player = collision.gameObject.GetComponent<Player>();
        if (player)
        {
            savedPlayer = player;
            cycleState = CycleState.ToDeactivate;
        }
    }

    public void ReleaseRope()
    {
        abseilPlayer.SetActive(false);
        savedPlayer.transform.position = abseilPlayer.transform.position + new Vector3(0, 0.6f, 0);
        savedPlayer.GetComponent<Rigidbody2D>().velocity = r2d.velocity;
        savedPlayer.gameObject.SetActive(true);
        savedPlayer.gameObject.GetComponent<PlayerInput>().ActivateInput();
        FindObjectOfType<Cinemachine.CinemachineVirtualCamera>().Follow = savedPlayer.transform;
        FindObjectOfType<Cinemachine.CinemachineVirtualCamera>().LookAt = savedPlayer.transform;
    }

    bool OnWall()
    {
        return wallDetector.IsTouchingLayers(layerMask) && playerCollider.IsTouchingLayers(layerMask);
    }

    public void Reset()
    {
        if (abseilPlayer.activeSelf)
        {
            ReleaseRope();
            savedPlayer.OnDie();
        }
    }

    Vector2 jumpVelocity()
    {
        Vector2 attachDir = attach.transform.position - abseilPlayer.transform.position;
        float radian = -85 * Mathf.Deg2Rad;
        return new Vector2(attachDir.x * Mathf.Cos(radian) - attachDir.y * Mathf.Sin(radian), attachDir.x * Mathf.Sin(radian) + attachDir.y * Mathf.Cos(radian)).normalized * jumpVelocityByTime.Evaluate(timeJump) * Time.fixedDeltaTime;
    }

    float timeMoveDown = 0f;
    float timeJump = 0f;

    void NotRunning()
    {
        if (cycleState == CycleState.ToDeactivate)
        {
            savedPlayer.gameObject.SetActive(false);
            abseilPlayer.SetActive(false);
            foreach (Rigidbody2D tr2d in savedPositions.Keys)
            {
                tr2d.isKinematic = true;
                tr2d.Sleep();
            }
            cycleState = CycleState.ToReset;
        }
        else if (cycleState == CycleState.ToReset)
        {
            foreach (KeyValuePair<Rigidbody2D, Vector2> keyvalue in savedPositions)
            {
                keyvalue.Key.velocity = Vector2.zero;
                keyvalue.Key.position = keyvalue.Value;
            }
            spring.distance = (attach.GetComponent<Rigidbody2D>().position - r2d.position).magnitude;
            cycleState = CycleState.ToActivate;

        }
        else if (cycleState == CycleState.ToActivate)
        {
            abseilPlayer.SetActive(true);
            foreach (Rigidbody2D tr2d in savedPositions.Keys)
            {
                tr2d.isKinematic = false;
                tr2d.WakeUp();
            }
            FindObjectOfType<Cinemachine.CinemachineVirtualCamera>().Follow = abseilPlayer.transform;
            FindObjectOfType<Cinemachine.CinemachineVirtualCamera>().LookAt = abseilPlayer.transform;
            cycleState = CycleState.Running;
        }
    }

    enum JumpState { PrepareJump, Jump };
    JumpState jumpState = JumpState.PrepareJump;
    void FixedUpdate()
    {
        if (cycleState != CycleState.Running)
        {
            NotRunning();
        }
        else
        {
            r2d.velocity += new Vector2(-wallMagnete * Time.fixedDeltaTime, 0);
            if (moveButton.y < -0.01f)
            {
                if (OnWall())
                {
                    spring.distance += rateOfFlowRopeOnWall * Time.fixedDeltaTime;
                    timeMoveDown = 0;
                }
                else
                {
                    spring.distance += rateOfFlowRopeByTime.Evaluate(timeMoveDown) * Time.fixedDeltaTime;
                    timeMoveDown += Time.fixedDeltaTime;
                }
            }
            else
                timeMoveDown = 0;
            if (moveButton.y > 0.01f)
            {
                spring.distance -= rateOfFlowRopeOnWall * Time.fixedDeltaTime;
                timeMoveDown = 0;
            }
            if (r2d.velocity.magnitude < 0.1 && Mathf.Abs(moveButton.y) > 0.01)
            {
                r2d.velocity += new Vector2(3f * Time.fixedDeltaTime, r2d.velocity.y);
            }
            if (jumpState == JumpState.PrepareJump && jumpButton && OnWall())
            {
                r2d.velocity += jumpVelocity();
                jumpState = JumpState.Jump;
            }
            else if (jumpState == JumpState.Jump)
            {
                if (jumpButton)
                {
                    r2d.velocity += jumpVelocity();
                    timeJump += Time.fixedDeltaTime;
                }
                else
                    jumpState = JumpState.PrepareJump;
            }
            else
                timeJump = 0;
        }
        if (_aimingManager.IsAiming && r2d.velocity.magnitude >= 0.01f)
            _aimingManager.DrawAimingLine();
    }
 }
