using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandOnRope : MonoBehaviour
{
    [SerializeField]
    Transform attach;

    [SerializeField]
    Transform harness;

    [SerializeField]
    float offset;

    void Update()
    {
        Vector3 direction = (attach.position - harness.position).normalized;
        transform.position = harness.position + direction * offset;
    }
}
