using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpRope : MonoBehaviour
{
    [SerializeField]
    Transform anchor;

    LineRenderer rope;

    void Start()
    {
        rope = GetComponent<LineRenderer>();
    }

    void Update()
    {
        rope.SetPosition(0, transform.position);
        rope.SetPosition(1, anchor.position);
    }
}
