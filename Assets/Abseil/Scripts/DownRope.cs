using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DownRope : MonoBehaviour
{
    [SerializeField]
    LayerMask layerMask;

    LineRenderer rope;

    void Start()
    {
        rope = GetComponent<LineRenderer>();
    }

    void Update()
    {
        rope.SetPosition(0, transform.position);
        RaycastHit2D raycastHit2D = Physics2D.Raycast(transform.position, Vector2.down, Mathf.Infinity, layerMask);
        rope.SetPosition(1, raycastHit2D.point);
    }
}
