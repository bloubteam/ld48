using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionParent : MonoBehaviour
{

    [SerializeField]
    [Range(0f, 5f)] // 5 sec maximal
    private float ExplosionDURATION = 2f;

    bool playerGotHit = false;

    IEnumerator DelateAfterDurationCoroutine()
    {
        Debug.Log("start coroutine in " + gameObject.name);

        yield return new WaitForSeconds(ExplosionDURATION);

        if(!playerGotHit)
            Destroy(gameObject);

        Debug.Log("finished coroutine");
    }

    void Start()
    {
        StartCoroutine(DelateAfterDurationCoroutine());
    }
    public void EventManager(int eventType)
    {
        if (eventType == 1) // Player touched the explosion
        {
            // delete the explosion
            Debug.Log("Player got hit by explosion!");
            // TODO player dies
            Destroy(gameObject);
        }
    }

}
