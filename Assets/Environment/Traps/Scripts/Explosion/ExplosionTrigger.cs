using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionTrigger : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == 3) // hit the player
        {
            GetComponentInParent<ExplosionParent>().EventManager(1);
        }
    }
}
