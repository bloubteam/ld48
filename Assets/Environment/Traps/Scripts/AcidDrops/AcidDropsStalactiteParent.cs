using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AcidDropsStalactiteParent : MonoBehaviour
{
    [SerializeField]
    [Range(0f, 5f)] // 5 sec maximal
    private float DURATION = 3f; // duration between each acidDrops fall

    private float _timeRemaining;
    Transform acidDropTf;

    [SerializeField]
    private GameObject prefabAcidDrop;

    private void Awake()
    {
        acidDropTf = transform.Find("AcidDropFrame");
        if (acidDropTf == null)
        {
            Debug.LogWarning("lsdlx: Cannot find the frame for acid drop!");
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        _timeRemaining = DURATION;
    }

    public void EventManager(int eventType)
    {
        switch (eventType)
        {
            case 1: // acid touched the ground
                // TODO Play "DropTouchTheGround"
                break;
            case 2: // acid touched the glowstick
                // instanciate an explosion at tf
                // do nothing
                break;
            case 3: // acid touched the player
                // TODO here player dies
                Debug.Log("Acid Touched the player");

                break;
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (_timeRemaining > 0)
        {
            _timeRemaining -= Time.deltaTime;
        }
        else
        {
            _timeRemaining = DURATION;
            DropDownAcid();

        }
    }
    void DropDownAcid()
    {
        GameObject acidDrop = Instantiate(prefabAcidDrop, transform); // change to Prefab
        acidDrop.transform.position = acidDropTf.position;
        //Rigidbody gameObjectsRigidBody = acidDrop.AddComponent<Rigidbody>();
        //gameObjectsRigidBody.constraints = RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationX;
    }


}
