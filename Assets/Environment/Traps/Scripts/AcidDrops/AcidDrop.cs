using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AcidDrop : MonoBehaviour
{
    [SerializeField]
    private GameObject prefabExplosion;

    [SerializeField] AudioSource _dropAudioSource;
    [SerializeField] AudioClip[] _dropClips;

    private int _numberOfAudioClip;
    private Player _player;

    private void Awake()
    {
        if (!_dropAudioSource)
            _dropAudioSource = GetComponent<AudioSource>();
        if (_dropClips != null)
            _numberOfAudioClip = _dropClips.Length;
        _player = FindObjectOfType<Player>();

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        switch (collision.gameObject.layer)
        {

            case 3:
                GetComponentInParent<AcidDropsStalactiteParent>().EventManager(2);
                _player.OnDie();
                _dropAudioSource.PlayOneShot(_dropClips[Random.Range(0, _numberOfAudioClip)]);
                Destroy(gameObject.GetComponent<Collider2D>());
                Destroy(gameObject.GetComponent<SpriteRenderer>());
                StartCoroutine(DelayBeforeDestroy());
                break;
            default:
                GetComponentInParent<AcidDropsStalactiteParent>().EventManager(1);
                _dropAudioSource.PlayOneShot(_dropClips[Random.Range(0, _numberOfAudioClip)]);
                Destroy(gameObject.GetComponent<Collider2D>());
                Destroy(gameObject.GetComponent<SpriteRenderer>());
                StartCoroutine(DelayBeforeDestroy());
                break;
                /* case 8:
                     Debug.Log("Acid touched a glowstick : EXPLOSION");
                     GetComponentInParent<AcidDropsStalactiteParent>().EventManager(3);
                     // create an explosion

                     GameObject explosion = Instantiate(prefabExplosion, GetComponentInParent<AcidDropsStalactiteParent>().transform);
                     explosion.transform.position = other.gameObject.transform.position; // location of the explosion at acid Drop position

                     Destroy(other.gameObject.GetComponentInParent<Glowstick>().gameObject); // Destroy the glowstick entirely (it should not light anymore also)
                     Destroy(gameObject); // Destroy the acid drop

                     break;*/

        }
    }

    private IEnumerator DelayBeforeDestroy()
    {
        while (_dropAudioSource.isPlaying)
            yield return new WaitForEndOfFrame();
        Destroy(gameObject);
    }
}
