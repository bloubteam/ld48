using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WagonParent : MonoBehaviour
{
    [SerializeField]
    [Range(0f, 1f)]
    private float PercentageOfMaxSpeed = 0.5f;

    [SerializeField]
    private float MaximalSpeed = 2f;

    [SerializeField]
    private bool ChangeStartDirectionMovement = false;

    private float _moveDirection = 0f;
    private Player _player;

    private Rigidbody2D _rigidbodyComponent;

    private void Awake()
    {
        _player = FindObjectOfType<Player>();
    }

    private void Start()
    {

        _rigidbodyComponent = GetComponentInChildren<Rigidbody2D>();

        if (ChangeStartDirectionMovement)
            _moveDirection = 1f;
        else
        {
            _moveDirection = -1f;
        }


        _rigidbodyComponent.velocity = new Vector2(MaximalSpeed * PercentageOfMaxSpeed * _moveDirection, _rigidbodyComponent.velocity.y);
    }



    /* Events for Wagon:
     * 5:  Wagon hit the player
     * 6:  Wagon turns in other direction (mesh change for wagon?)
     * 7:  
     * 8:
    */
    public void EventManager(int eventType)
    {
        switch (eventType)  // Player entered Zone 1
        {
            case 5:
                _player.OnDie();
                _rigidbodyComponent.velocity = new Vector2(MaximalSpeed * PercentageOfMaxSpeed * _moveDirection, _rigidbodyComponent.velocity.y);
                break;
            case 6:
                _moveDirection *= -1;
                _rigidbodyComponent.velocity = new Vector2(MaximalSpeed * PercentageOfMaxSpeed * _moveDirection, _rigidbodyComponent.velocity.y);
                break;
        }
    }

}
