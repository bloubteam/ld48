using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WagonSprite : MonoBehaviour
{
    private void Start()
    {
        Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Wagon"), LayerMask.NameToLayer("Default")); // Ignore Collision with default
        Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Wagon"), LayerMask.NameToLayer("Glowstick")); // Ignore Collision with glowstick sprite
        Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Wagon"), LayerMask.NameToLayer("GlowstickColliderForExplosion")); // Ignore Collisions with glowstick light
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player")) // the player hits the wagon sprite
        {
            GetComponentInParent<WagonParent>().EventManager(5);
        }

    }
}
