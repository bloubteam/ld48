using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WagonTurnAroundTrigger : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Wagon")) // the player hits the wagon sprite
        {
            GetComponentInParent<WagonParent>().EventManager(6);
        }
    }
}
