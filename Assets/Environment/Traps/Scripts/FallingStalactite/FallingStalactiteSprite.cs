using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingStalactiteSprite : MonoBehaviour
{
    private void Awake()
    {
        Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Default"), LayerMask.NameToLayer("Glowstick"));
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player")) // the player collide
        {
            GetComponentInParent<FalllingStalactiteParent>().PullOnTriggerEnter(3);
            return;
        }
        else
        {
            GetComponentInParent<FalllingStalactiteParent>().PullOnTriggerEnter(2);
        }
    }

}
