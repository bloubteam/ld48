using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingStalactiteTrigger : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Player")) // the player collide
        {
            GetComponentInParent<FalllingStalactiteParent>().PullOnTriggerEnter(1);
            Destroy(gameObject);
            Debug.Log("Delete the Trigger for the stalactite");
        }
    }

}
