using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FalllingStalactiteParent : MonoBehaviour
{
    [SerializeField]
    [Range(0f, 5f)] // 5 sec maximal
    public float DURATION = 2f; // serialize because depends on the sprite's height

    [SerializeField] private AudioSource _audioPlayer;
    [SerializeField] private AudioSource _fallingSpritePlayer;
    [SerializeField] private AudioClip _stalacticteBeginFallingSound;
    [SerializeField] private AudioClip _stalacticteFallSound;
    [SerializeField] private AudioClip _stalacticteHitSound;

    private float _timeRemaining;
    private bool hasCollide = false;
    private bool _isHarmless = false;
    private bool stalactiteHasHitSomething = false;

    private void Awake()
    {
        if (!_audioPlayer)
            _audioPlayer = GetComponent<AudioSource>();
        if (!_fallingSpritePlayer)
            _fallingSpritePlayer = transform.Find("FallingStalactiteSprite").GetComponent<AudioSource>();
    }

    public void PullOnTriggerEnter(int eventType)
    {
        switch (eventType)
        {
            case 1: // Player enter the zone
                _timeRemaining = DURATION; // 2 second delay before stalactite falls (default value for Prefabs)
                hasCollide = true;
                if (_stalacticteBeginFallingSound)
                    _audioPlayer.PlayOneShot(_stalacticteBeginFallingSound);
                break;
            case 2: // Stalactite collide with smth other than player
                if (!stalactiteHasHitSomething)
                {
                    _fallingSpritePlayer.Stop();
                    if (_stalacticteHitSound)
                        _fallingSpritePlayer.PlayOneShot(_stalacticteHitSound);
                    _isHarmless = true;
                }
                break;
            case 3: // Stalactite hit the player
                if (!stalactiteHasHitSomething && !_isHarmless)
                {
                    _fallingSpritePlayer.Stop();
                    if (_stalacticteHitSound)
                        _fallingSpritePlayer.PlayOneShot(_stalacticteHitSound);
                }
                break;
            case 4: // Stalactite starts falling
                    // TODO Fade in "StalactiteFalls"
                if (_stalacticteHitSound)
                    _fallingSpritePlayer.PlayOneShot(_stalacticteFallSound, 0.3f);
                break;

        }

    }

    // Update is called once per frame
    void Update()
    {
        if (hasCollide)
        {
            if (_timeRemaining > 0)
            {
                _timeRemaining -= Time.deltaTime;
            }
            else
            {
                hasCollide = false;

                // set gravity on the stalactite sprite

                //TODO Here play sound "StalactiteFalls"

                GameObject stalactiteSprite = GetComponent<Transform>().Find("FallingStalactiteSprite").gameObject;

                if (!stalactiteSprite)
                {
                    Debug.LogWarning("lsdlx: stalactiteSprite introuvable");
                    return;
                }

                // GameObject stalactiteSprite = GetComponent<Transform>().GetChild(0).gameObject;
                Rigidbody2D stalactiteRB = stalactiteSprite.GetComponent<Rigidbody2D>();
                stalactiteRB.simulated = true;
                PullOnTriggerEnter(4);

            }
        }

    }
}
