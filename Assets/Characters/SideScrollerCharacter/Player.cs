using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;
using Zenject;
using UnityEngine.Experimental.Rendering.Universal;

public class Player : MonoBehaviour
{
    [SerializeField]
    LayerMask layerMask;

    [SerializeField]
    Collider2D faceCollider;

    [SerializeField]
    Collider2D floorCollider;

    [SerializeField]
    Collider2D crounchCollider;

    [ReadOnly, SerializeField]
    float speed = 0;

    [SerializeField]
    float minWalkSpeed = 1.5f;

    [SerializeField]
    float maxWalkSpeed = 3f;

    [SerializeField]
    float runSpeed = 5;

    [SerializeField]
    float crounchSpeed = 1;

    [SerializeField]
    float timeBeforeRun = 2;

    [ReadOnly, SerializeField]
    float walkTime = 0;

    [SerializeField]
    Health health;

    [SerializeField]
    bool controlEnabled = true;

    [SerializeField]
    float jumpHeight = 10f;

    Rigidbody2D r2d;

    Animator animator;

    //En attendant la mise en place d'un GameManager
    [SerializeField]
    private Camera _sceneCamera = null;

    [SerializeField] private UIManager _uiManager;
    [SerializeField] private Transform _earTransform;

    private Lighter _lighter;



    private Camera _mainCamera;
    private float _currentGlowstickCooldownTime = 0f;
    private AimingManager _aimingManager;


    public Lighter PlayerLighter => _lighter;

    [Inject]
    readonly SignalBus signalBus;


    public void disableControls()
    {
        controlEnabled = false;
    }
    public void enableControls()
    {
        controlEnabled = true;
    }

    private void Awake()
    {
        if (!_uiManager)
            _uiManager = FindObjectOfType<UIManager>();
        if (!_earTransform)
            _earTransform = transform.Find("Ear");
        _lighter = GetComponent<Lighter>();
        _aimingManager = GetComponent<AimingManager>();
    }

    void Start()
    {
        r2d = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        _mainCamera = Camera.main;


        Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("Glowstick"), true);
        Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("GlowstickColliderForExplosion"), true);
    }

    Vector2 moveButton = Vector2.zero;
    public void OnMove(InputAction.CallbackContext context)
    {
        moveButton = context.ReadValue<Vector2>();
    }

    bool jumpButton = false;
    public void OnJump(InputAction.CallbackContext context)
    {
        if (context.started)
        {
            jumpButton = true;
        }
        if (context.canceled)
        {
            jumpButton = false;
        }
    }

  

    public void OnDie()
    {
        signalBus.Fire(new SpawnPlayer(gameObject));
    }







    bool crounchButton = false;
    public void OnCrounch(InputAction.CallbackContext context)
    {
        if (context.started)
        {
            crounchButton = true;
        }
        if (context.canceled)
        {
            crounchButton = false;
        }
    }

    bool isCrounch = false;

    void FixedUpdate()
    {
        bool isGrounded = floorCollider.IsTouchingLayers(layerMask);
        if (controlEnabled)
        {
            if (isCrounch || (crounchButton && isGrounded))
            {
                isCrounch = true;
                if (!crounchCollider.IsTouchingLayers(layerMask) && !crounchButton)
                    isCrounch = false;
            }
            if (moveButton.x > 0.01f)
            {
                transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, 0, transform.rotation.eulerAngles.z);
                _earTransform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, 0, transform.rotation.eulerAngles.z);
            }
            else if (moveButton.x < -0.01f)
            {
                transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, 180, transform.rotation.eulerAngles.z);
                _earTransform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, 0, transform.rotation.eulerAngles.z);
            }
                
            if (isGrounded)
                walkTime += Time.fixedDeltaTime;
            speed = Mathf.Lerp(minWalkSpeed, maxWalkSpeed, walkTime / timeBeforeRun);
            if (walkTime >= timeBeforeRun)
                speed = runSpeed;
            if (isCrounch)
                speed = crounchSpeed;
            if (moveButton.x == 0)
            {
                walkTime = 0;
                speed = 0;
            }
            r2d.velocity = new Vector2(Mathf.Sign(moveButton.x) * speed, r2d.velocity.y);
            if (_aimingManager.IsAiming && r2d.velocity.magnitude >= 0.01f)
                _aimingManager.DrawAimingLine();
            if (faceCollider.IsTouchingLayers(layerMask))
                r2d.velocity = new Vector2(0, r2d.velocity.y);

            if (!isCrounch && jumpButton && isGrounded)
            {
                r2d.velocity = new Vector2(r2d.velocity.x, jumpHeight);
                animator.SetTrigger("Jump");
            }
        }

        animator.SetBool("Falling", r2d.velocity.y < -0.5f && !isGrounded);
        animator.SetFloat("Speed", Mathf.Abs(r2d.velocity.x));
        animator.SetBool("Crounching", isCrounch);
    }

}
