using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlowstickManager : MonoBehaviour
{
    [SerializeField] private float _glowstickCooldownMax = 1f;
    [SerializeField] private UIManager _uiManager;

    private float _currentGlowstickCooldown = 0f;
    private int _numberRemaining = 5;

    public float CurrentGlowstickCooldown => _currentGlowstickCooldown;
    public int NumberRemaining => _numberRemaining;

    private void Awake()
    {
        if (!_uiManager)
            _uiManager = FindObjectOfType<UIManager>();
    }

    public void FireGlowsticks()
    {
        _numberRemaining = _numberRemaining - 1;
        _uiManager.SetGlowsticksRemaining(_numberRemaining);
        StartCoroutine(GlowStickCooldown());
    }

    public void RefillGlowsticks(int newNumber)
    {
        _numberRemaining = newNumber;
        _uiManager.SetGlowsticksRemaining(newNumber);
    }

    private IEnumerator GlowStickCooldown()
    {
        _currentGlowstickCooldown = _glowstickCooldownMax;
        _uiManager.SetGlowstickGrey(0.3f);

        while (_currentGlowstickCooldown > 0f)
        {
            _currentGlowstickCooldown = _currentGlowstickCooldown - Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        _uiManager.SetGlowstickGrey(1f);
        _currentGlowstickCooldown = 0f;
    }

}
