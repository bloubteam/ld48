using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPlayer
{
    public SpawnPlayer(GameObject player)
    {
        this.Player = player;
    }

    public GameObject Player
    {
        get; private set;
    }
}

public class SetNewSpawn
{
    public SetNewSpawn(GameObject spawn)
    {
        this.Spawn = spawn;
    }

    public GameObject Spawn
    {
        get; private set;
    }
}

